import nutils
import numpy
import unittest

'''
****************************************
File:           C3DP_KirchhoffLove.py
Author:         C. Adams
Project:        Concrete 3D printing
Project nr:     02000.02.01.21
****************************************
'''

class GeometryGenerator:
    
    @staticmethod
    def createScordelisLo( Nelem = 10 ):
        """
        createScordelisLo(): 
            Creates the full Scordelis-Lo roof geometry.
            Static method (no instance of class required).
        Arguments:
            Nelem: Nr. of elements per direction [-] (Nelem x Nelem mesh).
        """
        L = 50.
        R = 25.
        d, g = nutils.mesh.rectilinear([numpy.linspace(-L/2,+L/2,Nelem+1), numpy.linspace(-8/36*numpy.pi*R, 8/36*numpy.pi*R ,Nelem+1)])
        ns = nutils.function.Namespace()
        ns.R = R
        ns.s, ns.t = g
        ns.θ = 't / R' @ ns
        ns.X0_c = '<sin(θ) R, s, cos(θ) R>_c'
        
        return g, d, ns.X0
    
    @staticmethod
    def createCylinder( Nelem1 = 8, Nelem2 = 10, R = 250, H = 500 ):
        """
        createCylinder():
            Creates a cylindrical 3D geometry.
            Static method (no instance of class required).
        Arguments:
            Nelem1: Nr. of elements in circumferential direction [-].
            Nelem2: Nr. of elements in height direction [-].
            R: Cylinder radius [mm].
            H: Cylinder height [mm].
        """    
        d, g = nutils.mesh.rectilinear([numpy.linspace(0,2*numpy.pi*R,Nelem1+1), numpy.linspace(0,H,Nelem2+1)], periodic=[0])
        ns = nutils.function.Namespace()
        ns.R, ns.H = R,H
        ns.s, ns.t = g
        ns.θ = 's / R'
        ns.X0_c = '<R cos(θ), R sin(θ), t>_c'
        
        return g, d, ns.X0
    
    @staticmethod
    def createRectangle( Nelem1 = 5, Nelem2 = 5, L = 10, B = 15 ):
        """
        createReqtangle():
            Creates a rectangular 3D geometry.
            Static method (no instance of class required).
        Arguments:
            Nelem1: Nr. of elements in length direction [-].
            Nelem2: Nr. of elements in width direction [-].
            L: Rectangle length [mm].
            B: Rectangle width [mm].
        """
        d, g = nutils.mesh.rectilinear([numpy.linspace(0,L,Nelem1+1), numpy.linspace(0,B,Nelem2+1)])
        ns = nutils.function.Namespace()
        ns.s, ns.t = g
        ns.X0_c = '<s, t, 0>_c'
            
        return g, d, ns.X0



class KirchhoffLove:
    
    def __init__(self):
        """
        KirchhoffLove constructor:
            Initializes a KirchhoffLove instance and declares & defines essential variables.
        """
        self.sqr                = None
        self.historyCnt         = 0
        self.history            = []
        self.consExpr           = []
        self.consBnd            = []
        self.tracVecs           = []
        self.tracBnd            = []
        self.geometry           = False
        self.material           = False
        self.timedependent      = False
        self.plasticity         = False
        self.print              = False
        self.bodyLoad           = False
        self.tractionLoad       = False
        self.totallagrange      = False
        self.ns = nutils.function.Namespace( default_geometry_name='g', fallback_length=2 )
        
    def setGeometry( self, g, X, t ): 
        """
        setGeometry():
            Set geometrical variables in the KirchhoffLove instance.
        Arguments:
            g: Default nutils geometry (2D).
            X: Undeformed nutils geometry (3D position vectors).
            t: Shell thickness [mm].
        """
        self.g = g
        self.X0 = X
        self.t = t
        self.geometry = True
        
    def setMaterial( self, mattype = "isotropic", **kwargs ):
        """
        setMaterial():
            Set material related variables in the KirchhoffLove instance.
        Arguments:
            mattype: Material type (isotropic/time-dependent-isotropic).
            kwargs: Additional keyword arguments.
        """
        if mattype == 'isotropic':
            
            assert 'E' in kwargs, "setMaterial() ERROR: isotropic material: No E found."
            assert 'ν' in kwargs, "setMaterial() ERROR: isotropic material: No ν found."
            
            self.E = kwargs['E']
            self.ν = kwargs['ν']
            self.C = 'ν CONMET_ij CONMET_kl + 0.5 (1 - ν) ( CONMET_ik CONMET_jl + CONMET_il CONMET_jk )'
        
        # Time dependent, perfect plastic (non-hardening) isotropic material model for 3D printing process
        elif mattype == 'time-dependent-isotropic-3DP':
            
            assert 'ν' in kwargs, "setMaterial() ERROR: time-dependent-isotropic-3DP material: No ν found."
            assert 'iLay' in kwargs, "setMaterial() ERROR: time-dependent-isotropic-3DP material: No iLay found."
            assert 'tLay' in kwargs, "setMaterial() ERROR: time-dependent-isotropic-3DP material: No tLay found."
            assert 'ΔLay' in kwargs, "setMaterial() ERROR: time-dependent-isotropic-3DP material: No ΔLay found."
            assert 'A' in kwargs, "setMaterial() ERROR: time-dependent-isotropic-3DP material: No coefficient A found."
            assert 'B' in kwargs, "setMaterial() ERROR: time-dependent-isotropic-3DP material: No coefficient B found."
            
            self.ν  = kwargs['ν']
            iLay    = kwargs['iLay']
            tLay    = kwargs['tLay']
            ΔLay    = kwargs['ΔLay']
            A       = kwargs['A']
            B       = kwargs['B']
            
            # Compute print height dependent age t(H)
            ageExpr = str(tLay) + ' ( '+ str(iLay) +' + 1.5 - ( X0_2 / '+ str(ΔLay) +') )'
            
            # Set up linear time-dependent Young's modulus E(t(H)) = A*t(H)+B
            self.E = str(A) + ' (' + ageExpr + ') + ' + str(B)
            
            if self.historyCnt == 0:
                self.C = 'ν CONMET_ij CONMET_kl + 0.5 (1 - ν) ( CONMET_ik CONMET_jl + CONMET_il CONMET_jk )'
            
            # Set up linear time-dependent cohesion relation C(t(H)) = C*t(H)+D
            #C = kwargs['C']
            #D = kwargs['D']
            #self.coh = str(C) + ' (' + ageExpr + ') + ' + str(D)
            #self.Φ = str(kwargs['Φ'] * (numpy.pi/180)) # Angle of internal friction Φ
            #self.Ψ = str(kwargs['Ψ'] * (numpy.pi/180)) # Angle of dilatancy Ψ
            
            if 'plasticity' in kwargs:
                self.plasticity = True
            self.timedependent = True
            
            # TODO: Define self.C here as string.
                            
        else:
            raise TypeError('setMaterial() ERROR: Unknown material type.')
            
        self.material = True
        
    def addBodyLoad( self, q ): 
        """
        addBodyLoad():
            Adds a surface body load.
        Arguments:
            q: 3D body load vector as a string. Example: q = "<Qx, Qy, Qz>_c"
        """
        self.q = q
        self.bodyLoad = True
        
    def delBodyLoad( self ):
        """
        delBodyLoad():
            Deletes the surface body load.
        """
        self.bodyLoad = False
                
    def addTractionLoad( self, bnd, t ): 
        """
        addTractionLoad():
            Adds a traction load on a specified boundary.
        Arguments:
            bnd: Name of the boundary as a string. Example: bnd = "left,right,top,bottom"
            t: 3D traction load vector as a string. Example: t = "<Tx, Ty, Tz>_c"
        """
        self.tracVecs.append(t)
        self.tracBnd.append(bnd)
        self.tractionLoad = True
        
    def delTractionLoad( self ):
        """
        delTractionLoad():
            Deletes all traction loads.
        """
        self.tracVecs.clear()
        self.tracBnd.clear()
        self.tractionLoad = False
    
    def addDirichlet(self, bnd, component, val ):
        """
        addDirichlet():
            Adds dirichlet (displacement) boundary conditions on a specified boundary.
        Arguments:
            bnd: Name of the boundary as a string. Example: bnd = "left,right,top,bottom"
            component: Direction of the constraint. Example: "x".
            val: Displacement value [mm]. Example val = 0.15.
        """
        dirsindicator = {'x':'0','y':'1','z':'2'}
        u = 'u_' + dirsindicator[component]
        self.consExpr.append('(' + u + ' - ' + str(val) + ')^2')
        self.consBnd.append(bnd)
        
    def defineGlobalBasis(self, d, P = 2 ):
        """
        defineGlobalBasis():
            Construct shape functions across the total domain.
        Arguments:
            d: Nutils topology (2D).
            P: B-spline shape function order.
        """
        assert P > 1, "defineGlobalBasis() ERROR: B-spline basis requires a degree > 1."
        self.d = d
        self.P = P
        self.ns.basis = self.d.basis('spline', degree=P).vector(3)
        
    def set3DP(self, z, H ):
        """
        set3DP():
            Sets current 3D-printing process parameters & trims domain.
        Arguments:
            z: Current 3D print object height [mm].
            H: Final 3D print object height [mm].
        """
        print('\n ****** PRINTING PROCESS ' + str('{0:.3g}'.format((z/H)*100)) + ' [%] ******')        
        
        self.H = H
        self.z = z
        self.dcur = self.d.trim( (self.z - self.X0[2]), maxrefine=2, name='trimmed' )
        self.print = True
        
    def solveTotalLagrange(self, tol = 1.0e-6 ):
        """
        solveTotalLagrange():
            Solve the problem in a total Lagrangian setting (referring to the undeformed configuration).
        Arguments:
            tol: Solver tolerance.
        """
        self.totallagrange = True
        
        # No printing process
        if self.print is False:
            self.dcur = self.d
        
        assert self.geometry == True, 'solveTotalLagrange() ERROR: No geometry found. Please set geometry.'
        assert self.material == True, 'solveTotalLagrange() ERROR: No material found. Please set material.'
        assert self.plasticity == False, 'solveTotalLagrange() ERROR: For plasticity models, use solveUpdatedLagrange().'
                
        # Set in namespace
        self.ns.g = self.g
        self.ns.X0 = self.X0
        self.ns.t = self.t
        self.ns.ν = self.ν
        self.ns.E = self.E
                
        # Total displacement field
        self.ns.u_c = 'basis_nc ?lhs_n'
        
        # Deformed configuration
        self.ns.x_c = 'X0_c + u_c'
        
        # Covariant shell vecs (deformed g & undeformed G)
        self.ns.covG_ci = 'X0_c,i' 
        self.ns.covg_ci = 'x_c,i'
        
        # Undeformed metric tensors
        self.ns.COVMET_ij = 'covG_ci covG_cj'
        self.ns.CONMET = nutils.function.inverse( self.ns.COVMET )
        
        # Shell directors (deformed g3 & undeformed G3)
        self.ns.G3 = nutils.function.cross(self.ns.covG[:,0],self.ns.covG[:,1],axis=0)
        self.ns.G3 = self.ns.G3 / nutils.function.norm2(self.ns.G3)
        self.ns.g3 = nutils.function.cross(self.ns.covg[:,0],self.ns.covg[:,1],axis=0)
        self.ns.g3 = self.ns.g3 / nutils.function.norm2(self.ns.g3)
        
        # Green-Lagrange membrane strain [-]
        self.ns.ε_ij = '.5 (covg_ci covg_cj - covG_ci covG_cj)'
    
        # Green-Lagrange curvature [1/mm]
        self.ns.κ_ij = 'covG_ci,j G3_c - covg_ci,j g3_c'
        
        # Constitutive equation
        self.ns.C_ijkl = self.C
        
        # Material factors
        self.ns.cN = '(E t / (1 - ν^2))'
        self.ns.cM = '(E t^3) / (12 (1 - ν^2))'
        
        # Energy
        self.energy = self.dcur.integral('.5 C_ijkl ( cN ε_ij ε_kl + cM κ_ij κ_kl ) d:X0' @ self.ns, degree=2*self.P )

        if self.bodyLoad == True:
            self.ns.q_c = self.q
            self.energy -= self.dcur.integral('q_c u_c d:X0' @ self.ns, degree=2*self.P )
        
        if self.tractionLoad == True:
            Ntrac = len(self.tracVecs)
            for traction in range(Ntrac):
                self.energy -= self.dcur.boundary[self.tracBnd[traction]].integral(str(self.tracVecs[traction] + ' u_c d:X0') @ self.ns, degree=2*self.P )
                
        # Non-linear force residual
        self.res = self.energy.derivative('lhs')

        # Constraints
        Ncons = len(self.consExpr)
        for constraint in range(Ncons):
            if constraint == 0:
                self.sqr = self.dcur.boundary[self.consBnd[constraint]].integral( str(self.consExpr[constraint] + ' d:X0') @ self.ns, degree=2*self.P )
            else:
                self.sqr += self.dcur.boundary[self.consBnd[constraint]].integral( str(self.consExpr[constraint] + ' d:X0') @ self.ns, degree=2*self.P )
                
        self.dfic = (self.d - self.dcur)
        self.sqr += self.dfic.integral('u_c u_c d:X0' @ self.ns, degree=2*self.P)                
        self.cons = nutils.solver.optimize( 'lhs', self.sqr, droptol=1e-10, tol = 1e-10 )
        
        # Correct for the boundary
        if self.print and (self.z != self.H):
            self.dfic = (self.d - self.dcur)
            self.sqr  = self.dfic.boundary['trimmed'].integral('u_c u_c d:X0' @ self.ns, degree=2*self.P)
            self.corr = nutils.solver.optimize( 'lhs', self.sqr, droptol=1e-10, tol = 1e-10 )
            self.corr = numpy.isnan(self.corr)
            self.corr = numpy.logical_not(self.corr)
            
            for i in range(len(self.cons)):
                if self.corr[i]:
                    self.cons[i] = 'nan'

        # Coefficient vector (Non-linear Newton-Raphson procedure)
        self.lhs = nutils.solver.newton('lhs', self.res, constrain=self.cons).solve(tol=tol)
                
        # Second-Piola membrane & bending stress
        self.ns.N_ij = 'cN C_ijkl ε_kl' # Line load [N/mm]
        self.ns.M_ij = 'cM C_ijkl κ_kl' # Line moment [Nmm/mm] = [N]
        
        # Max. stress assumption
        self.ns.σ_ij = '(N_ij / t) + ((6 M_ij) / t^2)'
        
    
    def output(self, qoi, Nsample = 5 ):
        """
        output():
            Returns computed quantities.
        Arguments:
            qoi: A list of quantities of interest. Example: [u_0, u_2]
            Nsample: Nr. of post-processing sample points per element.
        """
        # Sampled domain
        self.sdcur = self.dcur.sample('bezier', Nsample)
        output = self.sdcur.eval(qoi @ self.ns, lhs=self.lhs)
        
        return output
        
    def export(self, VTKname, Nsample, mag ):
        """
        export():
            Exports the deformed configuration and displacement field as a VTK file.
        Arguments:
            VTKname: Name of the output file as a string.
            Nsample: Nr. of post-processing sample points per element.
            mag: Scale factor [-].
        """
        # Sampled domain
        self.sdcur = self.dcur.sample('bezier', Nsample)
                
        # Output displacement field
        self.ns.disp_c = 'u_c'
        
        # Scale the deformed configuration
        self.ns.mag = mag
        self.ns.deform_c = 'X0_c + mag disp_c'
        
        x, Ux, Uy, Uz, S11, S22, S12 = self.sdcur.eval(['deform_c', 'disp_0', 'disp_1', 'disp_2', 'σ_00', 'σ_11', 'σ_01'] @ self.ns, lhs=self.lhs)
        nutils.export.vtk( str('VTK/' + VTKname), self.sdcur.tri, x, Ux=Ux, Uy=Uy, Uz=Uz, S11=S11, S22=S22, S12=S12 )
        

        

        
        
    
        
'''
------------------------------------------------------------------------------------
-------------------------- Testsuite: Collection of tests --------------------------
------------------------------------------------------------------------------------
'''
def testSuite():
    suite = unittest.TestSuite()
    suite.addTest(TestShell('test_biaxial_tension'))
    suite.addTest(TestShell('test_plate_deflection'))
    suite.addTest(TestShell('test_beam_deflection'))
    #suite.addTest(TestShell('test_scordelislo'))

    return suite

class TestShell(unittest.TestCase):
    
    def test_biaxial_tension(self):
        """
        test_biaxial_tension():
            Square plate subjected to a bi-axial tension.
            B.C.    left: Ux = Uz = 0
                    right: Ux = Upres
                    top: Uy = Upres
                    bottom: Uy = Uz = 0
            Stress & strain field should be uniform.
        """
        # Input
        size            = numpy.pi
        Nelem           = 3
        P               = 2
        t               = numpy.pi/3
        E               = 210000
        ν               = 0.3
        Uprescribed     = 0.001
    
        # Geometry
        g, d, X = GeometryGenerator.createRectangle(Nelem,Nelem,size,size)
    
        # Numerical analysis
        KL = KirchhoffLove()
        KL.defineGlobalBasis(d,P)
        KL.setGeometry(g,X,t)
        KL.setMaterial(mattype = 'isotropic', E=E, ν=ν)
        KL.addDirichlet('left', 'x', 0.0)
        KL.addDirichlet('left', 'z', 0.0)
        KL.addDirichlet('bottom', 'y', 0.0)
        KL.addDirichlet('bottom', 'z', 0.0)
        KL.addDirichlet('right', 'x', Uprescribed)
        KL.addDirichlet('top', 'y', Uprescribed)
        KL.solveTotalLagrange(tol=1.0e-6)

        output = KL.output(['ε_00', 'ε_11', 'N_00', 'N_11'], Nsample=5)
        
        # Analytical analysis
        strainval = ((1+(Uprescribed/size))**2 - 1)/2
        εgl = numpy.array([strainval,strainval,0])
        
        # Plane stress stiffness matrix
        K = (E/(1-(ν**2))) * numpy.array([[1, ν, 0], [ν, 1, 0], [0, 0, (1-ν)/2]])
        
        # Second-Piola Kirchhoff stress [N/mm²]
        Ssp = numpy.dot(K, εgl)
        
        # Assert test
        self.assertAlmostEqual(max(abs(output[0])), abs(εgl[0]), places=3)
        self.assertAlmostEqual(max(abs(output[1])), abs(εgl[1]), places=3)
        self.assertAlmostEqual(max(abs(output[2]))/t, abs(Ssp[0]), places=3)
        self.assertAlmostEqual(max(abs(output[3]))/t, abs(Ssp[1]), places=3)
        
        # Log
        print('\n Analytical Green-Lagrange strain: ', abs(εgl[0]), ' [-]')
        print('Numerical Green-Lagrange strain: ', max(abs(output[0])), ' [-]')
        print('\nAnalytical Second-Piola Kirchhoff stress: ', abs(Ssp[0]), ' [N/mm²]')
        print('Numerical Second-Piola Kirchhoff stress: ', max(abs(output[2]))/t, ' [N/mm²]')
        
    def test_plate_deflection(self):
        """
        test_plate_deflection():
            S.S. square plate subjected to an out-of-plane bi-harmonic pressure.
            B.C.    left: Ux = Uz = 0
                    right: Ux = Uz = 0
                    top: Uy = Uz = 0
                    bottom Uy = Uz = 0
            q(x,y) = q0 sin(π x/L) sin(π y/B)
            Max. deflection in the center can be compared with an analytical value
        """
        # Input
        size    = 10*numpy.pi
        Nelem   = 10
        P       = 2
        t       = size/30
        E       = 210000
        ν       = 0.3
        q0      = - 0.5
                
        # Geometry
        L,B = size,size
        g, d, X = GeometryGenerator.createRectangle(Nelem,Nelem,L,B)
        
        # Body load vector
        π = numpy.pi
        qExpression = str(q0) + ' sin(' + str(π) + ' X0_0 / ' + str(L) + ') sin(' + str(π) + ' X0_1 / ' + str(B) + ')'
        
        # Numerical analysis
        KL = KirchhoffLove()
        KL.defineGlobalBasis(d,P)
        KL.setGeometry(g,X,t)
        KL.setMaterial(mattype = 'isotropic', E=E, ν=ν)
        KL.addBodyLoad(q = '<0, 0, ' + str(qExpression) + '>_c')
        KL.addDirichlet('left,right', 'x', 0.0)
        KL.addDirichlet('left,right', 'z', 0.0)
        KL.addDirichlet('top,bottom', 'y', 0.0)
        KL.addDirichlet('top,bottom', 'z', 0.0)
        KL.solveTotalLagrange(tol=1.0e-6)
        
        output = KL.output(['u_2'], Nsample=5)
        
        # Analytical analysis
        D = E*t**3/(12*(1-ν**2))
        wmax = (q0/(numpy.pi**4 * D)) * ((1/size**2) + (1/size**2))**(-2)
        
        # Assert
        self.assertAlmostEqual(max(abs(output[0])),abs(wmax),places=3)
        
        # Log
        print('\n Analytical max. plate deflection: ', abs(wmax), ' [mm]')
        print('Numerical max. plate deflection: ', max(abs(output[0])), ' [mm]')
        
    def test_beam_deflection(self):
        """
        test_beam_deflection():
            Strip of material subjected to out-of-plane tractions.
            B.C.    left: Ux = Uy = Uz = 0
                    right: Uy = Uz = 0
            t = t0 at top, bottom.
            Max. deflection can be compared with Euler-bernouilli beam theory.
        """
        
        # Input
        L       = 10*numpy.pi
        B       = L/10
        Nelem   = 15
        P       = 2
        t       = B/3
        E       = 210000
        ν       = 0.3
        t0      = -0.1
        
        # Geometry
        g, d, X = GeometryGenerator.createRectangle(Nelem,1,L,B)
        
        # Numerical analysis
        KL = KirchhoffLove()
        KL.defineGlobalBasis(d,P)
        KL.setGeometry(g,X,t)
        KL.setMaterial(mattype = 'isotropic', E=E, ν=ν)
        KL.addTractionLoad('top,bottom',t='<0, 0, ' + str(t0) + '>_c')
        KL.addDirichlet('left', 'x', 0.0)
        KL.addDirichlet('left,right', 'y', 0.0)
        KL.addDirichlet('left,right', 'z', 0.0)
        KL.solveTotalLagrange(tol=1.0e-6)
        
        output = KL.output(['u_2', 'M_00'], Nsample=5)

        # Analytical analysis
        q = 2*t0 # N/mm
        I = (1/12)*B*t**3
        wmax = 5/384 * (q*L**4)/(E*I)
        Mmax = (t0*L**2)/4
        σmax = (Mmax*(t/2))/I
        
        # Assert
        self.assertAlmostEqual(max(abs(output[0])), abs(wmax), places=3)
        
        # Log
        print('\nAnalytical max. deflection: ', abs(wmax), ' [mm]')
        print('Numerical max. deflection: ', max(abs(output[0])), ' [mm]')
        print('\nAnalytical max. moment: ', abs(Mmax), ' [Nmm]')
        print('Numerical max. moment: ', max(abs(output[1]))*B, ' [Nmm]')
        print('Analytical max. bending stress: ', abs(σmax), ' [N/mm²]')
        print('Numerical max. bending stress: ', 6*max(abs(output[1]))/(t**2), ' [N/mm²]')
        
    def test_loadstepping(self):
        """
        test_loadstepping():
            Strip of material subjected to a traction applied in multiple load steps.
            B.C.    left: Ux = Uy = Uz = 0
                    right: t = <Tx, 0, 0>_c
            Linear elasticity still used.
        """
        
        # Input
        L       = 10
        B       = 2
        Nelem1  = 10
        Nelem2  = 2
        P       = 2
        t       = 1.0
        T       = 500.0
        E       = 210000.0
        ν       = 0.3
        Nsteps  = 5
        
        # Geometry
        g, d, X = GeometryGenerator.createRectangle(Nelem1,Nelem2,L,B)
                    
        KL = KirchhoffLove()
        KL.defineGlobalBasis(d,P)
        KL.setGeometry(g,X,t)
        KL.setMaterial(mattype = 'isotropic', E=E, ν=ν)
        KL.addDirichlet('left', 'x', 0.0)
        KL.addDirichlet('left', 'z', 0.0)
        KL.addDirichlet('bottom', 'y', 0.0)
        KL.addDirichlet('bottom', 'z', 0.0)
        loadincr = 1/Nsteps
        
        for i in range(Nsteps):
            KL.addTractionLoad('right', t = '<' + str(loadincr * T) + ', 0, 0>_c')
            KL.solveTotalLagrange(tol=1.0e-6)
            KL.export('Total',10,1)

        
        result = KL.output(['u_0'],Nsample=10)
        Unumerical = max(result[0])
        
        # Analytical
        Uanalytical = ((((2*T)/(t*E)) + 1)**.5 - 1)*L
        
        print( "Analytical displacement: ", Uanalytical )
        print( "Numerical displacement: ", Unumerical )



    '''
    def test_scordelislo(self):
        """
        test_scordelislo():
            Scordelis-Lo roof test. Curved plate subjected to own weight.
            B.C.    left: Ux = Uz = 0
                    right: Ux = Uz = 0
            q = -90
        """
        
        # Input
        Nelem   = 10
        P       = 2
        E       = 4.32e8
        ν       = 0.
        t       = 0.25
        q0      = - 90
        
        # Geometry
        g, d, X = GeometryGenerator.createScordelisLo( Nelem )
        
        # Numerical analysis
        KL = KirchhoffLove()
        KL.defineGlobalBasis(d,P)
        KL.setGeometry(g,X,t)
        KL.setMaterial(mattype = 'isotropic', E=E, ν=ν)
        KL.addBodyLoad(q = '<0, 0, ' + str(q0) + '>_c')
        KL.addDirichlet('left,right', 'x', 0.0)
        KL.addDirichlet('left,right', 'z', 0.0)
        KL.solveTotalLagrange(tol=1.0e-6)
        KL.export('Scordelis-Lo', Nsample = 2, mag = 5)
    '''


'''
------------------------------------------------------------------------------------
-------------------------- Concrete 3D printing: Cylinder --------------------------
------------------------------------------------------------------------------------
'''
def C3DP_cylinder( Nc = 8, Nh = 10, P = 2, R = 500, H = 100, t = 100, ν = 0.21, ρ = 2.07e-6, Nlay = 5, Vprint = 10000, Nsample = 25 ):
    """
    C3DP_cylinder():
        3D printing of a concrete cylinder.
    Arguments:
        Nc: Nr. of elems in circumferential direction [-].
        Nh: Nr. of elems in height direction [-].
        P:  B-spline degree [-].
        R:  Cylinder radius [mm].
        H:  Cylinder height [mm].
        t:  Cylinder thickness [mm].
        ν: Poisson ratio [-].
        ρ: Concrete density [kg/mm³].
        Nlay: Nr. of 3D printed layers [-].
        Vprint: Print speed of the nozzle [mm/min].
        Nsample: Nr. of sample points per element [-].
    """
    # Geometry
    g, d, X = GeometryGenerator.createCylinder(Nc, Nh, R, H)
    
    KL = KirchhoffLove()
    KL.defineGlobalBasis(d,P)
    KL.setGeometry(g,X,t)

    # Add own weight load (as a surface load on the shell [N/mm²])
    ρ = ρ*t
    KL.addBodyLoad(q = '<0, 0, ' + '{:.10f}'.format(ρ) + '>_c')

    # Clamp the bottom
    KL.addDirichlet('bottom', 'x', 0.0)
    KL.addDirichlet('bottom', 'y', 0.0)
    KL.addDirichlet('bottom', 'z', 0.0)
    
    # Layer height
    ΔLay = H/Nlay
    
    # Layer print time [minutes]
    tLay = (2*numpy.pi*R)/Vprint

    # Loop over the print layers
    for iLay in range(Nlay):
                
        Z = (iLay + 1)*ΔLay
        KL.set3DP(Z,H)
        KL.setMaterial(mattype="isotropic", E=210000, ν=0.3)
        #KL.setMaterial(mattype = 'time-dependent-isotropic-3DP', ν=ν, A=1.227, B=71.78, iLay=iLay, tLay=tLay, ΔLay=ΔLay )
        
        KL.solveTotalLagrange(tol=1.0e-5)
        KL.export('C3DP-Total', Nsample = 10, mag = 1)

if __name__ == '__main__':
    
    # Run tests
    testrunner = unittest.TextTestRunner()
    testrunner.run(TestShell('test_loadstepping'))
    #testrunner.run(testSuite())
    
    # Run simulation
    #C3DP_cylinder( Nc=10, Nh=10, P=2, R=500, H=100, t=100, ν=0.21, ρ=2.07e-6, Nlay=3, Vprint=10000, Nsample=25 )

    
    
    
    