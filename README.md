# README #

Project: Concrete 3D printing.
Project number: 02000.02.01.21.



# Project background #
- 3D printing of concrete structures has a number of advantages, namely:
	- Increased geometrical freedom.
	- No formwork required.
	- Less physical labour.
	- Reduction of material usage = lower cost and environmental impact.
- Concrete 3D printing challenges:
	- During printing, the concrete is in the 'fresh' state and should be strong & stiff enough to avoid failure (buckling/plastic collapse).
	- After printing, the layered concrete structure is in the 'hardened'state, where the layers should adhere sufficiently to avoid failure.
- Project goal: Develop a numerical tool to analyse the 3D printing process & capture relevant failure mechanisms.
- Coding is done in Python, using Nutils as a finite element programming library. 



# Installation #
To install Python, Nutils & the C3DP code, follow the steps below:

- Install the Anaconda distribution (data science platform) from: https://www.anaconda.com/
- Install git via Anaconda:
	- Open Anaconda Prompt
	- Run 'conda install -c anaconda git'
- Install Nutils (latest):
	- Open Anaconda Prompt
	- Clone Nutils by running the command: git clone https://github.com/evalf/nutils.git [Clones into C:/nutils]
	- Install nutils by running the command: pip install --user --editable nutils
	- Run: git pull
- Install C3DP code:
	- Run: git clone https://camieladams@bitbucket.org/camieladams/c3dp_kirchhofflove.git
- Install Paraview:
	- Go to: www.paraview.org
	- Install Paraview (to post-process .VTK files)

See also: www.nutils.org.

# Numerical analysis example #

Square plate subjected to a bi-axial tension:


```Python
import nutils
import numpy

% Input
size            = numpy.pi 		# Plate edge size
Nelem           = 3 			# Nr. of elements per direction.
P               = 2 			# B-spline approximation order.
t               = numpy.pi/3 	# Plate thickness.
E               = 210000 		# Young's modulus.
ν               = 0.3 			# Poisson ratio.
Uprescribed     = 0.001 		# Prescribed displacement top & right.

% Create geometry
g, d, X = GeometryGenerator.createRectangle(Nelem,Nelem,size,size)

% Numerical analysis
KL = KirchhoffLove()
KL.defineGlobalBasis(d,P)
KL.setGeometry(g,X,t)
KL.setMaterial(mattype = 'isotropic', E=E, ν=ν)
KL.addDirichlet('left', 'x', 0.0)
KL.addDirichlet('left', 'z', 0.0)
KL.addDirichlet('bottom', 'y', 0.0)
KL.addDirichlet('bottom', 'z', 0.0)
KL.addDirichlet('right', 'x', Uprescribed)
KL.addDirichlet('top', 'y', Uprescribed)
KL.solveTotalLagrange(tol=1.0e-6)
KL.export('test_biaxial_tension', 10, 1)
```

# Lessons learnt / important #
- You should relate to the original geometry ('g') in order to calculate derivatives. You can do this by using the keyword argument ' default_geometry_name='g' ' in nutils.function.Namespace().
- Since the original geometry is used to compute derivatives, the original (flat) geometry from the nutils.mesh.rectilinear() function should have the actual dimensions of the final geometry (or use a jacobian?).
- Membrane & bending stresses are 'resultants' in [N/mm] & [Nmm/mm] and not the actual Second-Piola Kirchhoff stresses. There is an assumption for the maximum stress included in the formulation.
- Defining the basis (shape functions) on the complete geometry and then trimming a large part of it, led to ill-posed systems (low condition number = singular matrix errors).
- To circumvent the problem of having a low condition number, the basis is not defined on the whole domain, but on the trimmed ('print') domain. The finite cell method doesn't consider the elements that completely fall outside of the print domain, so only a small portion of the domain is trimmed. This results in a soluble system.
- Redefining the basis on the print domain, means that the number of degrees of freedom increases during a simulation. Nutils has problems with arguments (lhs) that have changing lengths and for this reason a new namespace (which entirely removes the old lhs argument) is created in each step in the non-linear simulation (each 'solve()' call).
- Nutils simulation are based on predefined governing 'expressions' which are, after solving the problem, evaluated in all sample points of the domain. This can be tricky sometimes.
- Since expressions (and the variables inside these expressions) are defined in namespaces, it is difficult to save e.g. a stress history when you have to redefine the namespace in each step. The stress history is required to compute the tangent stiffness matrix.



# Next steps #
- Include a plasticity model to capture the point where the material cannot sustain the self weight anymore (perfect plstic model in 'Non-associated plasticity for soils, concrete and rock' is advised). An Updated Lagrangian formulation is required here (already available in the code).
- Validate results of the cylindrical 3D print (including the plasticity model) with literature (PhD Thesis Rob Wolfs).
- Include actual (more complex) concrete material models.
- Include geometrical growth in the direction of the nozzle (not only layer-wise).
- Test other geometries.
- Create single basis on complete geometry, constrain away DOFs that do not contribute, instead of incrementally growing DOF vector.
